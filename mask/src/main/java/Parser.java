
public interface Parser {

    String parse(String file);
}
