

import com.google.gson.JsonParser;
import jregex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ParserJ2 implements Parser {
    private String holder;
    private Pattern combined;

    public ParserJ2() {
        this("***Censored***");
    }

    public ParserJ2(String censorString) {
        this.holder = censorString;
        this.combined = combinePatterns(new String[]{
                credit_card(), email(), ip(), ssn()});
    }

    public ParserJ2(String[] censorPatternsStr) {
        this();
        this.combined = combinePatterns(censorPatternsStr);

    }

    public ParserJ2(String censorString, String[] censorPatternsStr) {
        this.holder = censorString;
        this.combined = combinePatterns(censorPatternsStr);

    }

    private Pattern combinePatterns(String[] censorPatternsStr) {
        return new Pattern("^(?:" + String.join("|", censorPatternsStr) + ")$");
    }

    public String parse(String str) {
        Object obj = null;
        try {
            obj = new JSONParser().parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JsonParser jp = new JsonParser();
        String toReturn = "Invalid input";
        if (obj instanceof JSONArray) {
            Object[] objs = ((JSONArray) obj).toArray();
            for (Object o : objs
            ) {
                JSONObject jo = (JSONObject) o;
                censor(jo);
            }
            toReturn = ((JSONArray) obj).toJSONString();

        } else if (obj instanceof JSONObject) {
            JSONObject jo = (JSONObject) obj;
            censor(jo);
            toReturn = jo.toJSONString();

        } else {
            System.err.println("Invalid input");
        }
        return toReturn;
    }

    private boolean invalid(String toCheck) {
        return this.combined.matcher(toCheck).matches();
    }

    private void censor(JSONObject jo) {
        Object[] keysArray = jo.keySet().toArray();
        for (Object key : keysArray
        ) {
            Object keyvalue = jo.get(key);
            String keyStr = (String) key;
            //censor_key
            if (invalid(keyStr)) {
                jo.put(holder, jo.get(keyStr));
                jo.remove(keyStr);
            }
            if (keyvalue instanceof JSONObject) {
                censor((JSONObject) keyvalue);
            } else {
                //censor_value
                if (invalid(keyvalue.toString())) {
                    jo.put(key, holder);
                }
            }
        }
    }

    private String credit_card() {
        String Visa = "4[0-9]{12}(?:[0-9]{3})?";
        String MasterCard = "(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}";
        String AmericanExpress = "3[47][0-9]{13}";
        String DinersClub = "3(?:0[0-5]|[68][0-9])[0-9]{11}";
        String Discover = "6(?:011|5[0-9]{2})[0-9]{12}";
        String JCB = "(?:2131|1800|35\\d{3})\\d{11}";
        String[] cardTypes = {Visa, MasterCard, AmericanExpress, DinersClub, Discover, JCB};
        return String.join("|", cardTypes);
    }

    private String email() {
        return "[a-zA-Z0-9_!#$%&’*+/=?`\\|~^.-]+@[a-zA-Z0-9.-]+";
    }

    private String ip() {
        String zeroTo255
                = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";

        return zeroTo255 + "\\." + zeroTo255 + "\\."
                + zeroTo255 + "\\." + zeroTo255;
    }

    private String ssn() {
        return "(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}";
    }
}




