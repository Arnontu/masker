public class ParserS1 implements Parser {
    private String holder;
    private String combined;

    public ParserS1() {
        this("***Censored***");
    }

    public ParserS1(String censorString) {
        this.holder = censorString;
        this.combined = combinePatterns(new String[]{
                credit_card(), email(), ip(), ssn()});
    }

    public ParserS1(String[] censorPatternsStr) {
        this();
        this.combined = combinePatterns(censorPatternsStr);

    }

    public ParserS1(String censorString, String[] censorPatternsStr) {
        this.holder = censorString;
        this.combined = combinePatterns(censorPatternsStr);

    }

    private String combinePatterns(String[] censorPatternsStr) {
        return ("\\b(?:" + String.join("|", censorPatternsStr) + ")\\b");
    }

    public String parse(String str) {
        return str.replaceAll(this.combined, holder);
    }

    private String credit_card() {
        String Visa = "4[0-9]{12}(?:[0-9]{3})?";
        String MasterCard = "(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}";
        String AmericanExpress = "3[47][0-9]{13}";
        String DinersClub = "3(?:0[0-5]|[68][0-9])[0-9]{11}";
        String Discover = "6(?:011|5[0-9]{2})[0-9]{12}";
        String JCB = "(?:2131|1800|35\\d{3})\\d{11}";
        String[] cardTypes = {Visa, MasterCard, AmericanExpress, DinersClub, Discover, JCB};
        return String.join("|", cardTypes);

    }

    private String email() {
        return "[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+";
    }

    private String ip() {
        String zeroTo255
                = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";

        return zeroTo255 + "\\." + zeroTo255 + "\\."
                + zeroTo255 + "\\." + zeroTo255;
    }

    private String ssn() {
        return "(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}";

    }

}
