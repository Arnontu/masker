import java.io.StringWriter;
import java.util.HashSet;
import java.util.regex.Pattern;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class ParserX1 implements Parser {
    private String holder;
    private Pattern combined;

    public ParserX1() {
        this("***Censored***");
    }

    public ParserX1(String censorString) {
        this.holder = censorString;
        this.combined = combinePatterns(new String[]{
                credit_card(), email(), ip(), ssn()});
    }

    public ParserX1(String[] censorPatternsStr) {
        this();
        this.combined = combinePatterns(censorPatternsStr);

    }

    public ParserX1(String censorString, String[] censorPatternsStr) {
        this.holder = censorString;
        this.combined = combinePatterns(censorPatternsStr);

    }

    private Pattern combinePatterns(String[] censorPatternsStr) {
        return Pattern.compile("^(?:" + String.join("|", censorPatternsStr) + ")$");
    }

    private static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            return builder.parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getStringFromDocument(Document doc) {
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tf.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        try {
            assert transformer != null;
            transformer.transform(domSource, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public String parse(String xmlStr) {
        Document doc = convertStringToXMLDocument(xmlStr);
        assert doc != null;
        doc.getDocumentElement().normalize();
        censor(doc.getDocumentElement());
        return getStringFromDocument(doc);
    }

    private void censor(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            String val = node.getNodeName();
            if (invalid(val)) {
                node.setNodeValue(this.holder);
            }
            for (int i = 0; i < node.getChildNodes().getLength(); i += 1)
                censor(node.getChildNodes().item(i));
        } else {
            String val = node.getTextContent();
            if (invalid(val)) {
                node.setNodeValue(this.holder);
            }
        }
    }

    private String credit_card() {
        String Visa = "4[0-9]{12}(?:[0-9]{3})?";
        String MasterCard = "(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}";
        String AmericanExpress = "3[47][0-9]{13}";
        String DinersClub = "3(?:0[0-5]|[68][0-9])[0-9]{11}";
        String Discover = "6(?:011|5[0-9]{2})[0-9]{12}";
        String JCB = "(?:2131|1800|35\\d{3})\\d{11}";
        String[] cardTypes = {Visa, MasterCard, AmericanExpress, DinersClub, Discover, JCB};
        return String.join("|", cardTypes);

    }

    private String email() {
        return "[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+";

    }

    private String ip() {
        String zeroTo255
                = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";

        return zeroTo255 + "\\." + zeroTo255 + "\\."
                + zeroTo255 + "\\." + zeroTo255;
    }

    private String ssn() {
        return "(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}";
    }

    private Boolean invalid(String toCheck) {
        return this.combined.matcher(toCheck).matches();
    }

}




