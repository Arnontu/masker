import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ParserTests {
    private final String json_input = "src\\test\\json_example.json";
    private final String json_output = "src\\test\\json_result.json";

    private final String xml_input = "src\\test\\xml_example.xml";
    private final String xml_output = "src\\test\\xml_result.xml";

    private void TestThreadedParser(Class p, boolean renew, int[] numOfFiles, String input, String output, int[] numOfThreads) throws InterruptedException {

        String in = null;
        String out = null;
        try {
            in = new Scanner(new File(input)).useDelimiter("\\Z").next();
            out = new Scanner(new File(output)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Testing: " + p.getName() + ",\tRenew:" + renew);
        for (int curNumOfThreads : numOfThreads
        ) {
            System.out.println("Threads: " + curNumOfThreads);
            for (int curNumOfFiles : numOfFiles
            ) {
                Thread[] allThreads = new Thread[curNumOfThreads];
                long startTime = System.nanoTime();
                for (int j = 0; j < curNumOfThreads; j++) {
                    String finalContent = in;
                    String finalResult = out;
                    Thread t;
                    if (renew) {
                        t = new Thread(() -> {
                            for (int i = 0; i < curNumOfFiles / curNumOfThreads; i++) {
                                Object o = null;
                                try {
                                    o = p.newInstance();
                                } catch (InstantiationException | IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                                Parser parser = (Parser) o;
                                assert parser != null;
                                String res = parser.parse(finalContent);
                                assert finalResult != null;
                                if (!finalResult.equals(res)) {
                                    continue;
//                                    System.out.println("Censor is incorrect");
                                }
                            }
                        });
                    } else {
                        t = new Thread(() -> {
                            Object o = null;
                            try {
                                o = p.newInstance();
                            } catch (InstantiationException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            Parser parser = (Parser) o;
                            String res = "";
                            for (int i = 0; i < curNumOfFiles / curNumOfThreads; i++) {
                                assert parser != null;
                                res = parser.parse(finalContent);
                                assert finalResult != null;
                                if (!finalResult.equals(res)) {
                                    continue;
                                }
                            }
                        });

                    }

                    allThreads[j] = t;
                    t.start();
                }
                for (Thread t : allThreads
                ) {
                    if (t.isAlive()) {
                        t.join();
                    }
                }
                long duration = (System.nanoTime() - startTime) / 1000000;
                System.out.println("\tNumOfFiles: " + curNumOfFiles + "\t\tTime: " + duration + " ms\t\tPer File: " + duration / curNumOfFiles + " ms");
            }
        }

    }

    private void quickTestParser(Class p, String input, String output) throws InterruptedException {
        int[] numOfFiles = new int[]{100, 1000};
        int[] numOfThreads = new int[]{2, 4};
        TestThreadedParser(p, false, numOfFiles, input, output, numOfThreads);
    }

    private void TestParser(Class p, String input, String output) throws InterruptedException {
        int[] numOfFiles = new int[]{100, 1000, 10000};
        int[] numOfThreads = new int[]{1, 2, 4};
        TestThreadedParser(p, false, numOfFiles, input, output, numOfThreads);
    }


    @Test
    public void ParserJ1() throws InterruptedException {
        TestParser(ParserJ1.class, json_input, json_output);
    }


    @Test
    public void ParserJ2() throws InterruptedException {
        TestParser(ParserJ2.class, json_input, json_output);
    }


    @Test
    public void ParserSJ() throws InterruptedException {
        TestParser(ParserS1.class, json_input, json_output);
    }

    @Test
    public void ParserSX() throws InterruptedException {
        TestParser(ParserS1.class, xml_input, xml_output);
    }


    @Test
    public void ParserX1() throws InterruptedException {
        TestParser(ParserX1.class, xml_input, xml_output);
    }

    @Test
    public void quickJsonCompareParsers() throws InterruptedException {
        quickTestParser(ParserJ1.class, json_input, json_output);
        quickTestParser(ParserJ2.class, json_input, json_output);
        quickTestParser(ParserS1.class, json_input, json_output);
    }

    @Test
    public void quicXmlCompareParsers() throws InterruptedException {
        quickTestParser(ParserX1.class, xml_input, xml_output);
        quickTestParser(ParserS1.class, xml_input, xml_output);
    }

    @Test
    public void compareJsonParsers() throws InterruptedException {
        ParserJ1();
        ParserJ2();
        ParserSJ();
        ParserX1();
    }


}

